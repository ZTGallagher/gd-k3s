#!/bin/bash

#sudo rpm -ivh ./dependencies/*
#sudo rpm -ivh ./dependencies/k3s-selinux-0.1.1-rc1.el7.noarch.rpm
sudo yum -y --disablerepo=* localinstall dependencies/*.rpm
sudo cp ./bin/k3s /usr/local/bin/k3s
sudo mkdir -p /var/lib/rancher/k3s/agent/images/
sudo cp ./k3s-airgap-images-amd64.tar /var/lib/rancher/k3s/agent/images/

INSTALL_K3S_SKIP_DOWNLOAD=true K3S_URL='https://MASTER_NAME:6443' K3S_TOKEN=SECRET ./install-k3s.sh

K3S_TOKEN=SECRET k3s agent --server https://MASTER_NAME:6443
