#!/bin/bash

mkdir bin
mkdir dependencies
sudo yum install -y wget
sudo yum -y install --downloadonly --downloaddir=dependencies container-selinux
sudo yum -y reinstall --downloadonly --downloaddir=dependencies yum-utils device-mapper-persistent-data lvm2 selinux-policy-base
cd dependencies
wget https://rpm.rancher.io/k3s-selinux-0.1.1-rc1.el7.noarch.rpm
cd ..
wget https://github.com/rancher/k3s/releases/download/v1.19.1-rc1%2Bk3s1/k3s-airgap-images-amd64.tar
cd bin
wget https://github.com/rancher/k3s/releases/download/v1.19.1-rc1%2Bk3s1/k3s
chmod 775 k3s
cd ..
curl -v https://get.k3s.io/ > install-k3s.sh
chmod 775 install-k3s.sh