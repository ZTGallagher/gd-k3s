#!/bin/bash

sudo yum install -y vsftpd

sudo cp /etc/vsftpd/vsftpd.conf /etc/vsftpd/vsftpd.conf.bak

cat <<EOF | sudo tee /etc/vsftpd/vsftpd.conf
pam_service_name=vsftpd
userlist_enable=YES
anonymous_enable=NO
local_enable=YES
write_enable=YES
connect_from_port_20=YES
chroot_local_user=YES
user_sub_token=$USER
local_root=/home/$USER/
userlist_file=/etc/vsftpd/user_list
EOF

sudo systemctl restart vsftpd
