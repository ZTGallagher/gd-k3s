#!/bin/bash

sudo yum install -y firewalld git wget

sudo systemctl enable firewalld --now

for i in 2379 2380 6443 8080 10250 10251 10252
do
  sudo firewall-cmd --permanent --zone=public --add-port=${i}/tcp
done

sudo firewall-cmd --permanent --zone=public --add-port=8285/udp

sudo firewall-cmd --reload

mkdir git
cd git
git clone https://gitlab.com/ztgallagher/gd-k3s
cd gd-k3s
chmod 775 *.sh
chmod 775 testing-only/staging-script.sh
./testing-only/staging-script.sh
sed -i 's/MASTER_NAME/master1/g' HA-server-setup.sh