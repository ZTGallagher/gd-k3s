#!/bin/bash

#Unfinished script for generating certificates IF we are going with external DB
mkdir certificates
cd certificates
openssl genrsa -out k3s-ca.key 2048
openssl req -new -key k3s-ca.key -subj "/CN=KUBERNETES-CA" -out k3s-ca.csr
openssl x509 -req k3s-ca.csr -signkey ca.key -out k3s-ca.crt

openssl genrsa -out admin.key 2048
openssl req -new -key admin.key -subj "/CN=k3s-admin/O=system:masters" -out admin.csr
openssl x509 -req -in admin.csr -CA k3s-ca.crt -CAkey k3s-ca.key -out admin.crt


openssl genrsa -out etcd-server.key 2048
openssl req -new -key etcd-server.key -subj "/CN=kube-etcd" -out etcd-server.csr
openssl x509 -req -in etcd-server.csr -CA k3s-ca.crt -CAkey k3s-ca.key -out etcd-server.crt

cd ..