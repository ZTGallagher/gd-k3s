Instructions:

- Probably change K3S_TOKEN in the server, HA-server, and install scripts to something better. Just a string that links the nodes.
See Documentation for setting it to a file instead.

Run the server-installer.sh script on the "master" node, and the HA-server-setup.sh script on all subsequent master nodes. Worker/Compute nod
es get agent-installer.sh

That SHOULD be it.


Ports that need to be open.
TCP 2379/2380 etcd
TCP	6443	K3s agent nodes	Kubernetes API
UDP	8472	K3s server and agent nodes	Required only for Flannel VXLAN
TCP	10250	K3s server and agent nodes	kubelet

You can try this:

    for i in 2379 2380 6443 8080 10250 10251 10252
    do
      sudo firewall-cmd --permanent --zone=public --add-port=${i}/tcp
    done
    sudo firewall-cmd --permanent --zone=public --add-port=8285/udp
    sudo firewall-cmd --permanent --zone=public --add-port=8472/udp
    sudo firewall-cmd --reload




Manually pushing images to containerd

    ctr image pull <some-image-name> 
    # For example: docker.io/calico/node:v3.11.2

    ctr image export <output-filename> <image-name>
    # For Example ctr image export calico-node-v3.11.2.tar docker.io/calico/node:v3.11.2

    ctr -n=k8s.io images import <image-name.tar>
    # -n=k8s.io is necessary to ensure the image is available to the CRI for containerd

To ensure image is present and available to k3s, run:
    crictl images

IF you manually push images and IF you use "latest" tag, you MUST include "imagePullPolicy: IfNotPresent" to the container spec in a deployment otherwise ":latest" will always try to pull the image from an online registry.

Such as:

    spec:
      containers:
      - name: nginx
        image: docker.io/library/nginx:latest
        imagePullPolicy: IfNotPresent
        ports:
        - containerPort: 80


If a private registry is preferred, you need file "/etc/rancher/k3s/registries.yaml"

Examples would be:

    mirrors:
      docker.io:
        endpoint:
          - "https://mycustomreg.com:5000"
    configs:
      "mycustomreg:5000":
        auth:
          username: xxxxxx # this is the registry username
          password: xxxxxx # this is the registry password
        tls:
          cert_file: # path to the cert file used in the registry
          key_file:  # path to the key file used in the registry
          ca_file:   # path to the ca file used in the registry

Or,

    mirrors:
      docker.io:
        endpoint:
          - "http://mycustomreg.com:5000"
    configs:
      "mycustomreg:5000":
        auth:
          username: xxxxxx # this is the registry username
          password: xxxxxx # this is the registry password
